## testing with D / 20.02.2015

- oh, auch grün ist jetzt dabei!
- (beim Scrollen und Entdecken des "schwarzen" Ganges) Angst! ich weiss auch nicht warum...
- blau!

## testing with D / v2 / 18.01.2015

### what she is saying

- alle Wege mit Linien bemalen, "damit Käfer sich nicht verlaufen"
- "hier kann man ja gar nichts machen, nur malen und bewegen"
  - dann aber doch noch ca. 15 Minuten gespielt
- als die zwei Stellen kamen, wo sich die Struktur der Höhle ändern (Blätter, Verwischt)
  - oh, Angst, was mag da sein?
  - Gespänstergrube!
- oder eine unterirdische Stadt!?

### observations

- Schwierigkeiten mit Maus zu malen, beide Tasten zu bedienen
  - überhaupt: Maus schwierig, Tablet wäre wohl besser dafür