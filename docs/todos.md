# todos

- specify paintBucket-area more with a b/w-overlay-image
- show selected color on cursor-image
  - paint image with b/w-overlay
- how can I make the big background pictures smaller (greyscale)?
- change interface mode with mousewheel
- change input structure
  - forwarding input-events
- load & save information / feedback