# general

please exit only with ESC!!!

# controls

- keys
	- WASD & arrows: move
	- p: save painting
	- r: reset (delete all paintings)
	- space: toggle interface mode
	- ESC: exit
- mouse
	- interface mode 0
		- LEFT: move
		- RIGHT: paint
	- interface mode 1
		- RIGHT: paint
		- LEFT: move

# credits

...