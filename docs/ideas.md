# general ideas

- no save game, one game is one cave
  - the paintings are forever / permanent
- diferent versions possible
  - tablet, of course (controls?)
  - pc
- a game for lovers
  - hide your treasure somewhere in the cave and send the app to your partner

# level design

## themes / general ideas

- treasure cave on a pirate island
- dwarf caves
  - lord of the rings
  - inkl. all the buildings and cultural areas
- a real cave (as it is in cave_02)
  1. outside
  2. visiting area
  3. forbidden to enter (but in happens often)
  4. undiscovered areas
- a cave, where you have to start in the middle and find the way out

## details

- animal trails
- a broken / ripped-off map of the cave at the beginning
  - you can draw on it
- credits as a part of the cave
- a love nest
- water in the cave (can't paint on it)
- secret messages
  - area with invisible white lines, which can be made visible by painting over
- different paths with arows, leading to 
  - locations
  - in a loop
- "please don't paint in the cave"