
// ====================================
// code optimized for:
//   - export as OSX-app
//   - easy level design
//      - move / copy a map-folder into the same folder as app (after export)
//      - rename folder to "map"
// ====================================

Game game = new Game();

boolean tmpPaint = false;
boolean tmpDrag = false;

String mapToLoad = "map";


void setup()
{
  size(800, 600);

  // we need that for the multilayered scrolling of the images!
  imageMode(CENTER); 
  rectMode(CENTER);
  ellipseMode(CENTER);

  game.init(mapToLoad);
}

void draw() {
  background(255);
  game.draw();
}



void mousePressed() {

  // 0 - left: move around / right: paint

  if (game.interfaceMode == 0) {

    if (mouseButton == LEFT) {
      // start dragging
      tmpDrag = true;
    }

    if (mouseButton == RIGHT) {
      game.drawMapDot(game.screenXOnMap(mouseX), game.screenYOnMap(mouseY), game.brushC);
      tmpPaint = true;
      changeVisibleInterface(1);
    }
  }

  // 1 - left: paint / right: move around

  if (game.interfaceMode == 1) {

    if (mouseButton == LEFT) {
      // start painting
      tmpPaint = true;
      // draw the first dot
      game.drawMapDot(game.screenXOnMap(mouseX), game.screenYOnMap(mouseY), game.brushC);
    }

    if (mouseButton == RIGHT) {
      tmpDrag = true;
      changeVisibleInterface(0);
    }
  }

  // 

  if (tmpPaint) {
    if (game.mouseOverPaintBuckets()) {
      game.getDataFromMOPaintBucket();
      changeVisibleInterface(1);
    }
  }
}

void mouseDragged() {

  if (tmpDrag) {
    game.camXPos = game.camXPos - (mouseX - pmouseX);
    game.camYPos = game.camYPos - (mouseY - pmouseY);
  }

  if (tmpPaint) {
    game.drawMapLine(game.screenXOnMap(mouseX), game.screenYOnMap(mouseY), game.screenXOnMap(pmouseX), game.screenYOnMap(pmouseY), game.brushC);
  }
}

void mouseReleased() {

  if (tmpDrag) {
    if (mouseButton == LEFT && game.interfaceMode == 0) {
    }
    if (mouseButton == LEFT && game.interfaceMode == 1) {
    }
    if (mouseButton == RIGHT && game.interfaceMode == 0) {
    }
    if (mouseButton == RIGHT && game.interfaceMode == 1) {
      changeVisibleInterface(1);
    }
    tmpDrag = false;
  }

  if (tmpPaint) {
    if (mouseButton == LEFT && game.interfaceMode == 0) {
    }
    if (mouseButton == LEFT && game.interfaceMode == 1) {
    }
    if (mouseButton == RIGHT && game.interfaceMode == 0) {
      changeVisibleInterface(0);
    }
    if (mouseButton == RIGHT && game.interfaceMode == 1) {
    }
    tmpPaint = false;
  }
}

void keyPressed() {

  //println(keyCode);
  
  //r
  if (keyCode == 82){
    game.pImg = createImage(game.mapWidth, game.mapHeight, ARGB);
  }

  // p
  if (keyCode == 80) {
    savePaintingsInBetween();
  }

  int camSpeed = 10;

  // w & up
  if (keyCode == 87 || keyCode == 38) {
    game.camYPos = game.camYPos - camSpeed;
  }

  // a & left
  if (keyCode == 65 || keyCode == 37) {
    game.camXPos = game.camXPos - camSpeed;
  }

  // s & down
  if (keyCode == 83 || keyCode == 40) {
    game.camYPos = game.camYPos + camSpeed;
  }

  // d & right
  if (keyCode == 68 || keyCode == 39) {
    game.camXPos = game.camXPos + camSpeed;
  }

  // space
  if (keyCode == 32) {
    switchIntMode();
  }

  if (key==ESC) {
    key=0;
    savePaintingsExit();
    exit();
  }
}

void savePaintingsExit() {
  game.pImg.save(mapToLoad + "/paintings.png");
}

void savePaintingsInBetween() {

  int d = day();
  int mo = month();
  int y = year();

  int s = second();
  int mi = minute();
  int h = hour();

  game.pImg.save(mapToLoad + "/" + y + mo + d + "_" + h + mi + s + ".png");
}

void switchIntMode() {
  switch (game.interfaceMode) {
  case 0:
    activateIntMode (1);
    break;
  case 1:
    activateIntMode (0);
    break;
  }
}

void activateIntMode(int m) {

  switch (m) {
  case 0:
    game.interfaceMode = 0;
    changeVisibleInterface(0);
    break;
  case 1:
    game.interfaceMode = 1;
    changeVisibleInterface(1);
    break;
  }
}

void changeVisibleInterface(int m) {
  switch (m) {
  case 0:
    cursor(game.walkingUI);
    break;
  case 1:
    cursor(game.paintingUI, game.pUIx, game.pUIy);
    break;
  }
}

