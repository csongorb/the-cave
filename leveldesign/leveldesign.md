# Level Design

### Preparations

1. download & install [Dia](http://wiki.gnome.org/Apps/Dia/)
2. install [`thecaveDiaShapes/`](thecaveDiaShapes/)
   - copy the content of the above folder...
   - ...to the unvisible folder `~/.dia/` on your computer

### Location

The actual map is stored outside the application ([`../map/`](../map/)) for easier level editing & experimentation. All other maps / old maps / working copies / experiments should be saved in [`../leveldesign/maps/`](../leveldesign/maps/).

### Format

Take a look at the example ([`../map/`](../map/)). It should be self-explanatory.

- the main file is [`../map/map.dia`](../map/map.dia)
- the scale of the Dia-file is 1:10 (1 Dia unit is 10 pixels)
- pictures will be loaded / integrated at thier original size
   - Dia resizing will be overwritten