class MapRect {

  int xPos;
  int yPos;
  int wwidth;
  int hheight;
  color ccolor;

  MapRect() {
  }

  void draw() {
    drawRect(xPos, yPos, wwidth, hheight, ccolor);
  }

  void drawRect(int x, int y, int w, int h, color c) {

    // adjustment of the camera-midpoint for scrolling
    int xp = game.camXPos - (width / 2);
    int yp = game.camYPos - (height / 2);
    
    fill (c);
    noStroke();
    rect (x - xp, y - yp, w, h);
  }
}

