class Game {

  int mapWidth;
  int mapHeight;

  // placing the camera in the TOP LEFT corner
  int camXPos = width / 2;
  int camYPos = height / 2;

  XML map;
  PImage pImg;

  ArrayList<MapImg> mapImgsBack;
  ArrayList<MapImg> mapImgsFront;
  ArrayList<MapRect> mapRectsBack;
  ArrayList<MapRect> mapRectsFront;

  color brushC;
  ArrayList<PaintBucket> paintBuckets;

  PImage paintingUI;
  int pUIx;
  int pUIy;
  PImage defaultCursorImg;
  PImage walkingUI;

  int interfaceMode;

  Game() {
    mapImgsBack = new ArrayList<MapImg>();
    mapImgsFront = new ArrayList<MapImg>();
    mapRectsBack = new ArrayList<MapRect>();
    mapRectsFront = new ArrayList<MapRect>();
    paintBuckets = new ArrayList<PaintBucket>();
  }

  void init (String mapF) {

    defaultCursorImg = loadImage("UI/painting.png");
    paintingUI = defaultCursorImg;
    walkingUI = loadImage("UI/walking.png");

    loadMap (mapF);

    // 0 - left: move around / right: paint
    // 1 - left: paint / right: move around
    activateIntMode(1);
  }

  void draw() {

    keepCamOnMap();

    // paint images:
    // - back rectangles
    // - back images
    // - player paintings
    // - front images
    // - front rectangles

    for (int i = 0; i < mapRectsBack.size (); i++) {
      MapRect mr = mapRectsBack.get(i);
      mr.draw();
    }

    for (int i = 0; i < mapImgsBack.size (); i++) {
      MapImg mi = mapImgsBack.get(i);
      mi.draw();
    }

    drawPainting();

    for (int i = 0; i < mapImgsFront.size (); i++) {
      MapImg mi = mapImgsFront.get(i);
      mi.draw();
    }

    println(mapRectsFront.size());
    for (int i = 0; i < mapRectsFront.size (); i++) {
      MapRect mr = mapRectsFront.get(i);

      mr.draw();
    }
  }

  void drawPainting() {
    // I have to call this function so I can access the function drawImg in the other class
    MapImg mi = mapImgsFront.get(0); 
    mi.drawImg(pImg, mapWidth / 2, mapHeight / 2, pImg.width, pImg.height);
  }

  void loadMap(String mapFolder) {

    map = loadXML(mapFolder + "/map.dia");

    // scale of the dia-map 1:10
    int diaScaling = 10;

    // get layers

    XML[] mapLayers = map.getChildren("dia:layer");

    for (int i = 0; i < mapLayers.length; i++) {

      String layerName = mapLayers[i].getString("name");

      // layer: Background

      if (layerName.equals("Background")) {

        XML[] backgroundObjects = mapLayers[i].getChildren("dia:object");

        for (int ii = 0; ii < backgroundObjects.length; ii++) {

          String bgObjectType = backgroundObjects[ii].getString("type");

          if (bgObjectType.equals("Standard - Box")) {

            MapRect mr = new MapRect();
            mapRectsBack.add(mr);

            mr.wwidth = int(getDiaObjAttrFloat(backgroundObjects[ii], "elem_width") * diaScaling);
            mr.hheight = int(getDiaObjAttrFloat(backgroundObjects[ii], "elem_height") * diaScaling);

            mr.xPos = int((getDiaObjXPos(backgroundObjects[ii]) + (getDiaObjAttrFloat(backgroundObjects[ii], "elem_width") / 2)) * diaScaling);
            mr.yPos = int((getDiaObjYPos(backgroundObjects[ii]) + (getDiaObjAttrFloat(backgroundObjects[ii], "elem_height") / 2)) * diaScaling);

            color bColor = #000000;

            XML[] bgRectAttributes = backgroundObjects[ii].getChildren("dia:attribute");

            for (int iii = 0; iii < bgRectAttributes.length; iii++) {

              String bAttrName = bgRectAttributes[iii].getString("name");

              if (bAttrName.equals("inner_colour")) {
                XML XMLcHeight = bgRectAttributes[iii].getChild("dia:color");
                String bColorString1 = XMLcHeight.getString("val");
                String bColorString2 = "FF" + bColorString1.substring(1, 7);
                bColor = unhex(bColorString2);
              }
            }
            mr.ccolor = bColor;
          }

          if (bgObjectType.equals("Standard - Image")) {

            MapImg mi = new MapImg();
            mapImgsBack.add(mi);

            mi.wwidth = int(getDiaObjAttrFloat(backgroundObjects[ii], "elem_width") * diaScaling);
            mi.hheight = int(getDiaObjAttrFloat(backgroundObjects[ii], "elem_height") * diaScaling);

            mi.xPos = int((getDiaObjXPos(backgroundObjects[ii]) + (getDiaObjAttrFloat(backgroundObjects[ii], "elem_width") / 2)) * diaScaling);
            mi.yPos = int((getDiaObjYPos(backgroundObjects[ii]) + (getDiaObjAttrFloat(backgroundObjects[ii], "elem_height") / 2)) * diaScaling);

            String llink = getDiaObjAttrString(backgroundObjects[ii], "file");
            mi.iimage = loadImage(mapFolder + "/" + llink);
          }
        }
      }

      if (layerName.equals("Painting")) {

        XML[] bObjects = mapLayers[i].getChildren("dia:object");

        for (int ii = 0; ii < bObjects.length; ii++) {

          String bgObjectType = bObjects[ii].getString("type");

          if (bgObjectType.equals("Standard - Image")) {

            String llink = getDiaObjAttrString(bObjects[ii], "file");
            pImg = loadImage(mapFolder + "/" + llink);
          }
        }
      }

      if (layerName.equals("Foreground")) {

        XML[] foregroundObjects = mapLayers[i].getChildren("dia:object");

        for (int ii = 0; ii < foregroundObjects.length; ii++) {

          String fgObjectType = foregroundObjects[ii].getString("type");

          if (fgObjectType.equals("Standard - Box")) {

            MapRect mr = new MapRect();
            mapRectsFront.add(mr);

            mr.wwidth = int(getDiaObjAttrFloat(foregroundObjects[ii], "elem_width") * diaScaling);
            mr.hheight = int(getDiaObjAttrFloat(foregroundObjects[ii], "elem_height") * diaScaling);

            mr.xPos = int((getDiaObjXPos(foregroundObjects[ii]) + (getDiaObjAttrFloat(foregroundObjects[ii], "elem_width") / 2)) * diaScaling);
            mr.yPos = int((getDiaObjYPos(foregroundObjects[ii]) + (getDiaObjAttrFloat(foregroundObjects[ii], "elem_height") / 2)) * diaScaling);

            color bColor = #000000;

            XML[] bgRectAttributes = foregroundObjects[ii].getChildren("dia:attribute");

            for (int iii = 0; iii < bgRectAttributes.length; iii++) {

              String bAttrName = bgRectAttributes[iii].getString("name");

              if (bAttrName.equals("inner_colour")) {
                XML XMLcHeight = bgRectAttributes[iii].getChild("dia:color");
                String bColorString1 = XMLcHeight.getString("val");
                String bColorString2 = "FF" + bColorString1.substring(1, 7);
                bColor = unhex(bColorString2);
              }
            }
            mr.ccolor = bColor;
          }

          if (fgObjectType.equals("Standard - Image")) {

            MapImg mi = new MapImg();
            mapImgsFront.add(mi);

            mi.wwidth = int(getDiaObjAttrFloat(foregroundObjects[ii], "elem_width") * diaScaling);
            mi.hheight = int(getDiaObjAttrFloat(foregroundObjects[ii], "elem_height") * diaScaling);

            mi.xPos = int((getDiaObjXPos(foregroundObjects[ii]) + (getDiaObjAttrFloat(foregroundObjects[ii], "elem_width") / 2)) * diaScaling);
            mi.yPos = int((getDiaObjYPos(foregroundObjects[ii]) + (getDiaObjAttrFloat(foregroundObjects[ii], "elem_height") / 2)) * diaScaling);

            String llink = getDiaObjAttrString(foregroundObjects[ii], "file");
            mi.iimage = loadImage(mapFolder + "/" + llink);
          }
        }
      }

      if (layerName.equals("General")) {
        println(layerName);

        XML[] generalObjects = mapLayers[i].getChildren("dia:object");

        for (int ii = 0; ii < generalObjects.length; ii++) {

          String gObjectType = generalObjects[ii].getString("type");

          // get frame-data (map width & height)

          if (gObjectType.equals("Map Frame")) {
            mapWidth = int(getDiaObjAttrFloat(generalObjects[ii], "elem_width") * diaScaling);
            mapHeight = int(getDiaObjAttrFloat(generalObjects[ii], "elem_height") * diaScaling);
          }

          // get camera position

          if (gObjectType.equals("Camera (800x600)")) {
            camXPos = int((getDiaObjXPos(generalObjects[ii]) + (getDiaObjAttrFloat(generalObjects[ii], "elem_width") / 2)) * diaScaling);
            camYPos = int((getDiaObjYPos(generalObjects[ii]) + (getDiaObjAttrFloat(generalObjects[ii], "elem_height") / 2)) * diaScaling);
          }

          // get paintBuckets

          if (gObjectType.equals("Color Bucket")) {

            PaintBucket pb = new PaintBucket();
            paintBuckets.add(pb);

            pb.wwidth = int(getDiaObjAttrFloat(generalObjects[ii], "elem_width") * diaScaling);
            pb.hheight = int(getDiaObjAttrFloat(generalObjects[ii], "elem_height") * diaScaling);

            pb.xPos = int((getDiaObjXPos(generalObjects[ii]) + (getDiaObjAttrFloat(generalObjects[ii], "elem_width") / 2)) * diaScaling);
            pb.yPos = int((getDiaObjYPos(generalObjects[ii]) + (getDiaObjAttrFloat(generalObjects[ii], "elem_height") / 2)) * diaScaling);

            color bColor = #000000;

            XML[] bucketAttributes = generalObjects[ii].getChildren("dia:attribute");

            for (int iii = 0; iii < bucketAttributes.length; iii++) {

              String bAttrName = bucketAttributes[iii].getString("name");

              if (bAttrName.equals("line_colour")) {
                XML XMLcHeight = bucketAttributes[iii].getChild("dia:color");
                String bColorString1 = XMLcHeight.getString("val");
                String bColorString2 = "FF" + bColorString1.substring(1, 7);
                bColor = unhex(bColorString2);
              }
            }

            pb.paintC = bColor;

            String ci = getDiaObjAttrString(generalObjects[ii], "custom:CursorImg");
            pb.cursorImg = loadImage(mapFolder + "/" + ci);

            pb.cursorImgX = getDiaObjAttrInt(generalObjects[ii], "custom:CursorX");
            pb.cursorImgY = getDiaObjAttrInt(generalObjects[ii], "custom:CursorY");
          }
        }
      }
    }
  }

  float getDiaObjAttrFloat(XML obj, String attrName) {

    XML[] objAttributes = obj.getChildren("dia:attribute");
    float oAttr = 0;

    for (int i = 0; i < objAttributes.length; i++) {

      String objAttrName = objAttributes[i].getString("name");

      if (objAttrName.equals(attrName)) {
        XML objAttr = objAttributes[i].getChild("dia:real");
        oAttr = objAttr.getFloat("val");
      }
    }
    return oAttr;
  }

  int getDiaObjXPos(XML obj) {

    XML[] objAttributes = obj.getChildren("dia:attribute");
    int oXPos = 0;

    for (int i = 0; i < objAttributes.length; i++) {

      String objAttrName = objAttributes[i].getString("name");

      if (objAttrName.equals("obj_pos")) {
        XML XMLobjPos = objAttributes[i].getChild("dia:point");
        String objPos = XMLobjPos.getString("val");
        String[] objPositions = split(objPos, ',');
        oXPos = int(objPositions[0]);
      }
    }
    return oXPos;
  }

  int getDiaObjYPos(XML obj) {

    XML[] objAttributes = obj.getChildren("dia:attribute");
    int oYPos = 0;

    for (int i = 0; i < objAttributes.length; i++) {

      String objAttrName = objAttributes[i].getString("name");

      if (objAttrName.equals("obj_pos")) {
        XML XMLobjPos = objAttributes[i].getChild("dia:point");
        String objPos = XMLobjPos.getString("val");
        String[] objPositions = split(objPos, ',');
        oYPos = int(objPositions[1]);
      }
    }
    return oYPos;
  }

  String getDiaObjAttrString(XML obj, String attrName) {

    String cString = "";

    XML[] objAttributes = obj.getChildren("dia:attribute");

    for (int i = 0; i < objAttributes.length; i++) {

      String objAttrName = objAttributes[i].getString("name");

      if (objAttrName.equals(attrName)) {
        XML XMLobjCString = objAttributes[i].getChild("dia:string");
        String objCString = XMLobjCString.getContent();
        int stLength = objCString.length();
        cString = objCString.substring(1, stLength - 1);
      }
    }
    return cString;
  }

  int getDiaObjAttrInt(XML obj, String attrName) {

    int cInt = 0;

    XML[] objAttributes = obj.getChildren("dia:attribute");

    for (int i = 0; i < objAttributes.length; i++) {

      String objAttrName = objAttributes[i].getString("name");

      if (objAttrName.equals(attrName)) {

        XML XMLobjCString = objAttributes[i].getChild("dia:int");
        cInt = XMLobjCString.getInt("val");
      }
    }
    return cInt;
  }

  boolean mouseOverPaintBuckets() {
    for (int i = 0; i < paintBuckets.size (); i++) {
      PaintBucket pb = paintBuckets.get(i);
      if (pb.mouseOver()) {
        return true;
      }
    }
    return false;
  }

  int getMouseOverPaintBucketNr() {
    for (int i = 0; i < paintBuckets.size (); i++) {
      PaintBucket pb = paintBuckets.get(i);
      if (pb.mouseOver()) {
        return i;
      }
    }
    return 0;
  }

  void getDataFromMOPaintBucket() { 

    int mouseOverBucket = game.getMouseOverPaintBucketNr();
    PaintBucket pBucket = game.paintBuckets.get(mouseOverBucket);

    game.brushC = pBucket.paintC;
    game.paintingUI = pBucket.cursorImg;
    game.pUIx = pBucket.cursorImgX;
    game.pUIy = pBucket.cursorImgY;
  }


  void drawMapPoint(int x, int y, color c) {
    if (x >= 0 && x < mapWidth && y >= 0 && y < mapHeight) {
      pImg.loadPixels();
      pImg.pixels[x + (y * pImg.width)] = c;
      pImg.updatePixels();
    }
  }

  void drawMapDot(int x, int y, color c) {

    drawMapPoint(x, y, c);
    drawMapPoint(x - 1, y, c);
    drawMapPoint(x + 1, y, c);
    drawMapPoint(x, y - 1, c);
    drawMapPoint(x, y + 1, c);
  }

  void drawMapLine(int x1, int y1, int x2, int y2, color c) {

    int xDist = x2 - x1;
    int yDist = y2 - y1;
    float d = dist (x1, y1, x2, y2);

    float fx = xDist / d;
    float fy = yDist / d;

    drawMapDot(x1, y1, c);
    for (int i = 1; i < int (d); i++) {
      drawMapDot(x1 + int(fx * i), y1 + int(fy * i), c);
    }
    //drawMapDot(x2, y2, c);
  }

  int screenXOnMap(int x) {
    int sx = x + (camXPos - (width / 2));
    return sx;
  }

  int screenYOnMap(int y) {
    int sy = y + (camYPos - (height / 2));
    return sy;
  }

  void keepCamOnMap () {

    // --------------

    if (camXPos < 0 + (width / 2)) {
      camXPos = 0 + (width / 2);
    }
    if (camXPos > mapWidth - (width / 2)) {
      camXPos = mapWidth  - (width / 2);
    }

    // --------------

    if (camYPos <= 0 + (height / 2)) {
      camYPos = 0 + (height / 2);
    }
    if (camYPos > mapHeight - (height / 2)) {
      camYPos = mapHeight - (height / 2);
    }
  }
}

