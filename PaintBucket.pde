class PaintBucket {

  int xPos;
  int yPos;
  int wwidth;
  int hheight;
  color paintC;
  PImage cursorImg;
  int cursorImgX;
  int cursorImgY;

  PaintBucket() {
    cursorImg = game.defaultCursorImg;
  }

  boolean mouseOver() {
    if (game.screenXOnMap(mouseX) > xPos - (wwidth / 2)) {
      if (game.screenXOnMap(mouseX) < xPos + (wwidth / 2)) {
        if (game.screenYOnMap(mouseY) > yPos - (hheight / 2)) {
          if (game.screenYOnMap(mouseY) < yPos + (hheight / 2)) {
            return true;
          }
        }
      }
    }
    return false;
  }
  
}

