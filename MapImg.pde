class MapImg {

  PImage iimage;
  int xPos;
  int yPos;
  int wwidth;
  int hheight;

  MapImg() {
  }

  void draw() {
    drawImg(iimage, xPos, yPos, wwidth, hheight);
  }

  void drawImg(PImage i, int x, int y, int w, int h) {

    // adjustment of the camera-midpoint for scrolling
    int xp = game.camXPos - (width / 2);
    int yp = game.camYPos - (height / 2);

    image (i, x - xp, y - yp, w, h);

    //draw boder?
    //noFill();
    //rect (x - xp + xOffset, y - yp + yOffset, w, h);
  }
    
}

